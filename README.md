La Capi TV
==========

Una televisión interespacial

Un servicio de streaming intergaláctico donde podés encontrar animaciones de La Capi entre tantas otras series y pequeños experimentos de animación.

~~~
cordova platform add android
cordova plugin cordova-plugin-advanced-http
cordova plugin cordova-plugin-auth-dialog
cordova plugin cordova-plugin-file
cordova plugin cordova-plugin-inappbrowser
cordova build android
~~~

Apk release
===========

~~~
cordova build android --release -- --packageType=apk
~~~

AppImage y Wine
===============

~~~
cordova platform add electron
cordova build electron
~~~

